const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    search(search) {
      const item = FighterRepository.getOne(search);
      if (!item) {
        return null;
      }
      return item;
    }
    
    create(user) {
      const item = FighterRepository.create(user);
      if (!item) {
        return null;
      }
      return item;
    }
  
    update(id, fighterToUpdate) {
      const item = FighterRepository.update(id, fighterToUpdate);
      if (!item) {
        return null;
      }
      return item;
    }
  
    delete(id) {
      FighterRepository.delete(id);
    }
    
    getAll() {
      const items = FighterRepository.getAll();
      if (!items) {
        return null;
      }
      return items;
    }
  
}

module.exports = new FighterService();