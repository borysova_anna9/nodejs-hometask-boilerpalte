const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    create(user) {
        const item = UserRepository.create(user);
        if (!item) {
          return null;
        }
        return item;
      }
    
      update(id, userToUpdate) {
        const item = UserRepository.update(id, userToUpdate);
        if (!item) {
          return null;
        }
        return item;
      }
    
      delete(id) {
        UserRepository.delete(id);
      }
    
      getAll() {
        const items = UserRepository.getAll();
        if (!items) {
          return null;
        }
        return items;
      }
        //add check
}

module.exports = new UserService();