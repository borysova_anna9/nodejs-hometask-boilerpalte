const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/api/fighters', (req, res, next) => {
    try {
        const items = FighterService.getAll();
        res.data = items;
      } catch (err) {
        res.err = err.message;
      }
      next();
}, responseMiddleware);

router.get('/api/fighters/:id', function(req, res, next ) {
    try {
        const id = req.params.id;
        const item = FighterService.search({id});
        if (item) {
            res.data = item;
        } 
        else  {
            throw Error("Doesn`t retrieve a data");
        }    
    } catch (err) {
        res.err = err.message;
      } finally {
        next();
      }
}, responseMiddleware);

router.post('/api/fighters', createFighterValid, (req, res, next) => {
    if (!res.err) {
        const fighter = wrapper(sanitizeID)(FighterService.create)(req.body);
        res.data = fighter;
    }
    next();
}, responseMiddleware);

router.put( "api/fighters/:id", updateFighterValid, (req, res, next) => {
      try {
        const id = req.params.id;
        const item = FighterService.update(id, req.body);
        if (item) {
            res.data = item;
        }
        else {
            throw Error("Doesn`t retrieve a data");
        }
        
      } catch (err) {
        res.err = err.message;
      } finally {
        next();
      }
    },
    responseMiddleware
  );

router.delete('/api/fighters/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        FighterService.delete(id);
        res.data = null;
    } 
    catch (err) {
        res.err = err.message;
    }
    finally {
        next();
    }
    
},responseMiddleware);
module.exports = router;