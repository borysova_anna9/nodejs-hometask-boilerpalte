const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get("/api/users", (req, res, next) => {
  try {
    const items = UserService.getAll();

    if (items) res.data = items;
    else throw Error("Doesn`t retrieve a data");
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

router.get("/api/users/:id", (req, res, next) => {
  try {
    const id = req.params.id;
    const item = UserService.search({ id });
    if (item) {
       res.data = item;
    }
    else {
      throw Error("Incorrect id of user");
    }
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

router.post("/api/users", createUserValid, (req, res, next) => {
  if (!res.err) {
    const user = wrapper(sanitizeID)(UserService.create)(req.body);

    res.data = user;
  }
  next();
}, responseMiddleware);

router.put("/api/users/:id", updateUserValid, (req, res, next) => {
  try {
    const id = req.params.id;
    const data = UserService.update(id, req.body);
    if (data) {
      res.data = data;
    } 
    else {
      throw Error("User not found");
    }
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete("/api/users/:id", (req, res, next) => {
  try {
    const id = req.params.id;
    UserService.delete(id);
    res.data = null;
  } catch (err) {
    res.err = err.message;
  } finally {
    next();
  }
}, responseMiddleware);
  
module.exports = router;
  