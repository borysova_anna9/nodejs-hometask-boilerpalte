const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {
        let error = "";
    
        console.log(req.body);
    
        for (let key in fighter) {
          error += FighterService.checkFields(key, req.body, fighter);
        }
    
        req.body.health = 100;
    
        if (error !== "") res.err = "Fighter creation error: " + error;
      } catch (err) {
        res.err = "Some fields don`t exist";
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        let error = "";
    
        const { name, power, defense } = req.body;
        req.body = { name, power, defense };
    
        for (let key in req.body) {
          error += FighterService.checkFields(key, req.body, user);
        }
    
        if (error !== "") res.err = "Fighter creation error: " + error;
      } catch (err) {
        res.err = "Some fields are missed";
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;