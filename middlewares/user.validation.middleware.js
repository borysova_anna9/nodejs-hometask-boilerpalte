const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        let error = "";
    
        for (let key in user) {
          error += UserService.checkFields(key, req.body, user);
        }
    
        if (error !== "") res.err = "Registration error: " + error;
      } catch (err) {
        res.err = "Some fields are missed";
      }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        let error = "";
        for (let key in user) {
          error += UserService.checkFields(key, req.body, user);
        }
    
        if (error !== "") res.err = "Update error: " + error;
      } catch (err) {
        res.err = "Doesn`t initialized all fields";
      }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;