const validator = require("validator");

exports.user = {
  id: "",
  firstName: {
    type: "String",
    validator: validator.isAlpha,
    errorMessage: "First name must have only letters "
  },
  lastName: {
    type: "String",
    validator: validator.isAlpha,
    errorMessage: "Last name must have only letters "
  },
  email: {
    type: "Email",
    validator: checkEmail,
    pattern: "(@gmail.com)$",
    errorMessage: "Email must have form example@gmail.com "
  },
  phoneNumber: {
    type: "Phone",
    validator: validator.isMobilePhone,
    max:13,
    code: "uk-UA",
    errorMessage: "Phone number must have 13 digits and start with +380 "
  },
  password: {
    type: "Number",
    validator: validator.isLength,
    min: 3,
    errorMessage: `Password must have more than 3 symbols `
  }
};

function checkEmail(data, pattern) {
  return data.match(pattern);
}
